from time import time
from time import ctime
import logging


class CacheFlag:
    """
    A simple class for managing the cache lifetime Flags.
    Notes:
         1. Whenever the checkExpired method of the object is called the flag is set correctly if cacheLifeTime has expired.
         2. The cacheLastUpdate time refers to the last time the Cache has been set or updated.
    """
    # todo - to add a decorator  function to be used as a default caller to any function that  is going to use the CacheFlag class
    # todo - to refine the CacheFlag class in a way that every instance of it can keep track to
    #        which cache field is associated and to have a function that will be capable to refresh the cache
    #        from inside the the object. Currently once the cache has expired we start calling only the external apis
    # todo - to add nametuples in order to have an easy access to the flag from the wrapper function.
    def __init__(self, cacheFlagValue = True, cacheLifeTime = 60, flagName = None):
        if type(cacheFlagValue) != bool:
            # warnings.warn(e, category=CommonCheckAlarm)
            return None
        self.__flagName__= flagName
        logging.debug('cache flagName: %s' % self.__flagName__)
        self.cacheFlag = cacheFlagValue
        self.cacheLifeTime = cacheLifeTime
        self.cacheLastUpdate = time()
        self.currentTime = time()
        logging.debug("%s cacheLastUpdate set to: %s" % (self.__flagName__,
                                                         ctime(self.cacheLastUpdate)))

    def __call__(self):
        self.checkExpired()
        return self.cacheFlag

    def __setCurrentTime__(self):
        self.currentTime = time()

    # gets the current value of the flag - it is not necessary the same value as the one returned from self.checkExpired()
    def __get__(self):
        return self.cacheFlag

    # artificially setting the flag without checking if the cacheLifeTime has Expired
    def __set__(self, cacheFlagValue):
        self.cacheFlag = cacheFlagValue
        return self.cacheFlag

    # checks if the cacheLifeTime has expired
    def checkExpired(self):
        self.__setCurrentTime__()
        if self.currentTime - self.cacheLastUpdate > self.cacheLifeTime:
            self.cacheFlag = True
            return self.cacheFlag
        self.cacheFlag = False
        return self.cacheFlag

    # Checks the if the cacheLifeTime has expired and sets the cacheLastUpdate time
    def checkExpiredAndSet(self):
        # logging.debug("checkExpiredAndSet called for cache flagName: %s" % self.__flagName__)
        if self.checkExpired():
            self.cacheLastUpdate = time()
            logging.debug("%s cacheLastUpdate set to: %s" % (self.__flagName__,
                                                             ctime(self.cacheLastUpdate)))
        return self.cacheFlag

class Cache():

    """
    The class that implements the cache for the Overflow.
    static  - long living information
    dynamic - short living information
    """

    def __init__ (self, cacheType):
        self.cacheType = cacheType
        pass
        


